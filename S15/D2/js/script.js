//get the user button with and id of btn1 using querySelector

let btn1 =  document.querySelector("#btn1");

// add an eventlistener that calls a prompt box

btn1.addEventListener("click", function(){
	//the value entered in the prompt is saved in userInput
	let userInput = prompt ("Enter Name");
	//get the element with an id of output and write the value of userInput
	document.getElementById('output').innerHTML = userInput;
})