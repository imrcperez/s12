//Alert Window
/*alert("Hello from JS");
alert("Hello from JS");
alert("Hello from JS");
alert("Hello from JS");*/

/*JavaScirpt syntax is the set of rules.
How JavaScirpt programs are constructed.*/

//statements are isntructions that we tell the computer to perform. Js statements usually end with semi colon
// console.log("Hello World");
// console.log("Using JavaScirpt");

//Variables - are containers for data.
// let fname = "Peter Griffin";
// console.log(fname);

// fname = "Home Simpsons";
// console.log(fname);

// fname = 'Ironman';
// // alert(fname);
// console.log("Tony Stark");
// let greeting;
// greeting = "Hello! We're here!";
// console.log(greeting);

// greeting = '"She said Yes!"';
// console.log(greeting);

// let money = 200;
// console.log(money);

//Vairable and Data Types
// String
// Number

// let name = "Peter Griffin"; //string
// let num1 = 12;				//numeric
// let num2 = 3; 				//numeric

// console.log(name + " Family Guy");

//String length property
// console.log(name.length);
// console.log(name[4]);

// console.log(num1 + num2);

//Assignment Operator
// let x = 2;
// let y = 3;

// console.log(x); 
// //expected 2

// console.log(x = y+1); //3+1
// //expected output 4

// console.log (x = x*y); // 4*3
// //expected output 12

//Addition Assignment Operator

// let bar = 5

// bar +=2 //7
// //expected output is 7
// console.log(bar);

// //Substraction Assignment Operator

// let bar = 5

// bar -=2 //3
// //expected output is 3
// console.log(bar);

// let bar = 5

// bar *=2 //10
// //expected output is 10
// console.log(bar);

// let bar = 5

// bar /=2 //2.5
// //expected output is 2.5
// console.log(bar);

// Comaprison Operator
// console.log(1 ==1); // true

// console.log("1"==1); // false

// console.log(1 ===1); // true

// console.log("1"===1); // false

// console.log(1 !=2); // true

// console.log(1 !="1"); // false

// console.log(3 !=='3'); // true

// console.log(4 !==3); // true

// let x = 4;
// let y = 3;
// console.log( x > y); //true

// 

// console.log( y >= x); //false

// console.log( x < y); // false

// console.log( x <= y); //false

// console.log( x <=10); // true

//logical operator | OR, && (AND), !(NOT)

// console.log((19>11) && (23<50)); // true

// console.log((19>21) || (23<50)); // true

// console.log((19>11) || (23===50)); // true

//logical not (!)

 // console.log(!(100<150)) // false

 //conditional statements

 






