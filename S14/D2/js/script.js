// let movies = ["Godfather", "Mission Impossible", "Avengers", "Venom", "The Nun"];

// console.log(movies[0])
// console.log(movies[1]);
// console.log(movies[2]);

// movies[1] = "Halloween";
// console.log(movies[1]);
// console.log(movies.length) //  length is 5

// //we can initialize anb empty array two ways

// let color = [];
// let colors = new Array() // uncommon

// //Arrays can hold any type of data
// let random_collection = [48, true,"Hermoine", null];

// //Arrays have a length property

// let nums = [45, 37, 89 , 24];
// nums.length //4

//push

// let colors = ["red", "orange", "yellow"];

// console.log(colors) // output is ["red", "orange", "yellow"]

// colors.push("green");
// console.log(colors) // output is ["red", "orange", "green"]

// let colors = ["red", "orange", "yellow"];

// console.log(colors) // output is ["red", "orange", "yellow"]

// colors.pop(); // removed yellow
// console.log(colors) // output is ["red", "orange"];

//unshift

// let colors = ["red", "orange", "yellow"];

// colors.unshift("infrared"); // ["infrared","red", "orange", "yellow"];
// console.log(colors);

//shift

// let colors = ["red", "orange", "yellow"];

// console.log(colors.shift()); // shift returns the remove item

// console.log(colors);

// let tuitt = ["Charles", "Paul", "Sef", "Alex", "Paul"];

// //returns the first index at which a given element can be found

// console.log(tuitt.indexOf("Sef")); //2

// //finds the first instance of Paul 1 not 4
// console.log(tuitt.indexOf("Paul")); // 1

// //returns -1 if the element is not present

// console.log(tuitt.indexOf("Hulk")); // -1



// let colors = 
// ["red", "orange", "yellow", "green"];

// for(let i = 0 ; i < colors.length; i++)
// {
// 	console.log(colors[i]);
// }

let colors = 
["red", "orange", "yellow", "green"];

colors.forEach(function(color){
//color is a placeholder
	console.log(color);

});





